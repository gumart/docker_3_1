<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Test;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/app', function () {
    return 'app1';
});

Route::get('/get-all-test-strings', function (Request $request) {

    // $result = Test::query()->where('id', $id)->first();
    $result = [];
    foreach (Test::all() as $test){
        $result[] = $test->test_string;
    }

    return $result;
});

Route::get('/set-test-string/{test_string}', function ($test_string) {
    $test = Test::create([
        'test_string' => $test_string
    ]);

    return $test->test_string;
});
