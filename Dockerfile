FROM php:8.1.0-fpm-alpine3.15 as build

RUN apk update && apk upgrade && apk add curl\
    && docker-php-ext-install pdo_mysql \
    && curl -sS https://getcomposer.org/installer -o composer-setup.php \
    && php composer-setup.php --install-dir=/usr/local/bin --filename=composer \
    && rm composer-setup.php

COPY . /var/www/html

WORKDIR /var/www/html

RUN composer update

FROM php:8.1.0-fpm-alpine3.15

COPY --from=build --chown=www-data:www-data /var/www/html /var/www/html

RUN docker-php-ext-install pdo_mysql 

RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

WORKDIR /var/www/html/
